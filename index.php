<?php
{ // CORS
	$origin = array_key_exists('HTTP_ORIGIN', $_SERVER) ? $_SERVER['HTTP_ORIGIN'] : '*';
	if (!$origin) {
		$origin = '*';
	}

	$headerAllow = array_key_exists('HTTP_ACCESS_CONTROL_REQUEST_HEADERS', $_SERVER) ? $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'] : '*';
	if (!$headerAllow) {
		$headerAllow = '*';
	}

	$headers = [ //
		'Access-Control-Allow-Origin' => $origin, //
		'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS', //
		'Access-Control-Allow-Credentials' => 'true', //
		'Access-Control-Max-Age' => '3600', //
		'Access-Control-Allow-Headers' => $headerAllow //
	];

	foreach ($headers as $header => $value) {
		header("$header: $value");
	}

	if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
		exit();
	}
}

function getOr($key, $array, $default = null) {
	if (is_array($array) && array_key_exists($key, $array)) {
		return $array[$key];
	}
	return $default;
}

function getClientIP() {
	$ipHeaders = ['HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', //
		'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR'];

	foreach ($ipHeaders as $ipHeader) {
		if (array_key_exists($ipHeader, $_SERVER)) {
			return $_SERVER[$ipHeader];
		}
	}
	return "UNKNOWN";
}

function fixLine($line) {
	if ($line === null) {
		return null;
	}
	$line = str_replace(',', '.', $line);
	if (strpos($line, '.') !== false) {
		$lineExplode = explode('.', $line);
		while (strlen($lineExplode[1]) < 4) {
			$lineExplode[1] = '0' . $lineExplode[1];
		}
		$line = $lineExplode[0] . '.' . $lineExplode[1];
	}
	return $line;
}


header('Content-Type: application/json');

try {
	$DB = new PDO("mysql:host=store.makertim.nl;dbname=lighthouse", //
		'Lighthouse', //
		trim(file_get_contents('.password')), [ //
			PDO::ATTR_PERSISTENT => true, //
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, //
		]);

	$headers = getallheaders();
	$output = [];
	switch ($_SERVER['REQUEST_METHOD']) {
		case 'GET'/*Get all Applications / Get all Bugs*/
		:
			if (array_key_exists('Application', $headers)) {
				if (!array_key_exists('Secret', $headers)) {
					$output['_Order'] = [];
					foreach ($DB->query('SELECT id, name, owner FROM Application ORDER BY name')->fetchAll(PDO::FETCH_NAMED) as $row) {
						$id = $row['id'];
						unset($row['id']);
						$output[$id] = $row;
						$output['_Order'][] = $id;
					}
					break;
				}
				$secret = $headers['Secret'];
				$name = $headers['Application'];

				$statement = $DB->prepare('' .//
					'SELECT i.ID as iID, r.ID AS rID, `From`, Meta, Agent, Environment, Version, Origin, `When`, `Type`, `Message`, `File`, `Line`, `Stacktrace`, `Solved_Since`, Screenshot ' . //
					'FROM Incident i ' . //
					'LEFT JOIN Report r ' . //
					'ON i.Report_ID = r.ID ' . //
					'WHERE Application_id = ' . //
					'(SELECT ID FROM Application ap ' . //
					'WHERE ap.SecretKey = :key ' . //
					'AND ap.Name = :name) ORDER BY i.When DESC');
				$statement->bindParam(':key', $secret);
				$statement->bindParam(':name', $name);
				$statement->execute();

				$output['_Order'] = [];
				$output['_Count'] = 0;

				foreach ($statement->fetchAll(PDO::FETCH_NAMED) as $row) {
					if (!in_array($row['rID'], $output['_Order'])) {
						$output['_Order'][] = $row['rID'];
					}
					if (isset($output[$row['rID']])) {
						$currentRow = $output[$row['rID']];
					} else {
						$currentRow = $output[$row['rID']] = (object)[ //
							'Type' => $row['Type'], //
							'Message' => $row['Message'], //
							'File' => $row['File'], //
							'Line' => preg_replace('/\.0+/', ':', $row['Line']), //
							'Stacktrace' => $row['Stacktrace'], //
							'Solved_Since' => $row['Solved_Since'], //
							'Incidents' => new stdClass(), //
							'Incidents_Order' => [], //
						];
					}
					$rowId = $row['iID'];
					$currentRow->Incidents_Order[] = $rowId;
					$currentRow->Incidents->$rowId = [ //
						'From' => $row['From'], //
						'Meta' => $row['Meta'], //
						'Agent' => $row['Agent'], //
						'Environment' => $row['Environment'], //
						'Version' => $row['Version'], //
						'Origin' => $row['Origin'], //
						'When' => $row['When'], //
						'Screenshot' => $row['Screenshot'], //
					];
					$output['_Count']++;
				}
				break;
			} else {
				header('Location: ./admin.php');
				exit();
			}
		case 'POST'/*Push new Bug*/
		:
			if (array_key_exists('Application', $headers)) {
				if (array_key_exists('Incident', $headers)) {
					if (!array_key_exists('Key', $headers)) {
						$output['fail'] = 'K-P-I';
						break;
					}
					$secret = $headers['Key'];
					$name = $headers['Application'];

					$statement = $DB->prepare(' ' . //
						'SELECT ID FROM Application ap ' . //
						'WHERE ap.Key = :key ' . //
						'AND ap.Name = :name');
					$statement->bindParam(':key', $secret);
					$statement->bindParam(':name', $name);
					$statement->execute();
					if (!$statement->rowCount()) {
						$output['fail'] = 'U-P-I';
						break;
					}
					$aID = $statement->fetch()['ID'];

					$body = json_decode(file_get_contents('php://input'), true);

					$type = getOr('Type', $body, '');
					$message = getOr('Message', $body, '');
					$file = getOr('File', $body, '');
					$line = fixLine(getOr('Line', $body, ''));
					$stacktrace = getOr('Stacktrace', $body, '');

					$statement = $DB->prepare(' ' . //
						'SELECT ID ' . //
						'FROM Report ' . //
						'WHERE IFNULL(`Type`, \'\') = :Type ' . //
						'AND   IFNULL(`Message`, \'\') = :Message ' . //
						'AND   IFNULL(`File`, \'\') = :File ' . //
						'AND   IFNULL(`Line`, \'\') = :Line ' . //
						'AND   IFNULL(`Stacktrace`, \'\') = :Stacktrace');
					$statement->bindParam(':Type', $type);
					$statement->bindParam(':Message', $message);
					$statement->bindParam(':File', $file);
					$statement->bindParam(':Line', $line);
					$statement->bindParam(':Stacktrace', $stacktrace);
					$statement->execute();

					if ($statement->rowCount()) {
						$rID = $statement->fetch()['ID'];
					} else {
						$type = getOr('Type', $body);
						$message = getOr('Message', $body);
						if (strlen($message) >= 500) {
						    $message = substr($message , 0, 500);
						}
						$file = getOr('File', $body);
						$line = fixLine(getOr('Line', $body));
						$stacktrace = getOr('Stacktrace', $body);

						$statement = $DB->prepare(' ' . //
							'INSERT INTO Report (`Type`, `Message`, `File`, `Line`, `Stacktrace`) VALUES (:Type, :Message, :File, :Line, :Stacktrace)');
						$statement->bindParam(':Type', $type);
						$statement->bindParam(':Message', $message);
						$statement->bindParam(':File', $file);
						$statement->bindParam(':Line', $line);
						$statement->bindParam(':Stacktrace', $stacktrace);
						$statement->execute();
						$rID = $DB->lastInsertId('ID');
					}

					$from = getClientIP();
					$meta = getOr('Meta', $body);
					$agent = getOr('User-Agent', $headers);
					$environment = getOr('Environment', $body);
					$version = getOr('Version', $body);
					$origin = getOr('Origin', $body, getOr('Referer', $headers, getClientIP()));
					$screenshot = getOr('Screenshot', $body);

					$statement = $DB->prepare(' ' . //
						'INSERT INTO Incident (`Report_ID`, `Application_ID`, `From`, `Meta`, `Agent`, `Environment`, `Version`, `Origin`, `Screenshot`) VALUES (:rID, :aID, :From, :Meta, :Agent, :Environment, :Version, :Origin, :Screenshot);');
					$statement->bindParam(':rID', $rID);
					$statement->bindParam(':aID', $aID);
					$statement->bindParam(':From', $from);
					$statement->bindParam(':Meta', $meta);
					$statement->bindParam(':Agent', $agent);
					$statement->bindParam(':Environment', $environment);
					$statement->bindParam(':Version', $version);
					$statement->bindParam(':Origin', $origin);
					$statement->bindParam(':Screenshot', $screenshot);
					$statement->execute();

					$output['ID'] = rtrim(base64_encode('R' . $rID . '-I' . $DB->lastInsertId('ID')), '=');
				} else if (array_key_exists('Handle', $headers)) {
					if (!array_key_exists('Secret', $headers)) {
						$output['fail'] = 'S-P-H';
						break;
					}
					if (!array_key_exists('Rid', $headers)) {
						$output['fail'] = 'I-P-H';
						break;
					}
					$secret = $headers['Secret'];
					$name = $headers['Application'];

					$statement = $DB->prepare(' ' . //
						'SELECT ID FROM Application ap ' . //
						'WHERE ap.SecretKey = :key ' . //
						'AND ap.Name = :name');
					$statement->bindParam(':key', $secret);
					$statement->bindParam(':name', $name);
					$statement->execute();
					if (!$statement->rowCount()) {
						$output['fail'] = 'U-P-H';
						break;
					}
					$aID = $statement->fetch()['ID'];

					$statement = $DB->prepare('' . //
						'SELECT r.ID ' . //
						'FROM Report r ' . //
						'LEFT JOIN Incident i ' . //
						'ON i.Report_ID = r.ID ' . //
						'WHERE r.ID = :rID AND i.Application_ID = :aID');
					$statement->bindParam(':rID', $headers['Rid']);
					$statement->bindParam(':aID', $aID);
					$statement->execute();
					if (!$statement->rowCount()) {
						$output['fail'] = 'V-P-H';
						break;
					}

					if ($headers['Handle'] === 'SOLVE') {
						$statement = $DB->prepare('UPDATE Report SET Solved_Since = CURRENT_TIMESTAMP() WHERE ID = :ID');
					} else if ($headers['Handle'] === 'DELETE') {
						$statement = $DB->prepare('DELETE FROM Report WHERE ID = :ID');
					} else if ($headers['Handle'] === 'UNSOLVE') {
						$statement = $DB->prepare('UPDATE Report SET Solved_Since = NULL WHERE ID = :ID');
					} else {
						$output['fail'] = 'H-P-H';
						break;
					}
					$statement->bindParam(':ID', $headers['Rid']);
					$statement->execute();
					$output['success'] = true;
				}
			}
			break;
		default:
			$output['fail'] = 'U';
	}
	//
	echo json_encode((object)$output, JSON_PRETTY_PRINT);
	//
} catch (\Exception $exception) {
	echo json_encode([ //
		'error' => true, //
		'error_class' => get_class($exception), //
		'error_msg' => $exception->getMessage(), //
		'error_file' => $exception->getFile(), //
		'error_line' => $exception->getLine(), //
	], JSON_PRETTY_PRINT);
} finally {
	$DB->query('SELECT 1;')->fetchAll();
}
