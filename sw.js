const cacheName = new Date().toISOString().split('T')[0].substr(0, 7);

self.addEventListener('install', event => {
	event.waitUntil(
		caches
			.open(cacheName)
			.then(cache =>
				cache.addAll([
					'./admin.php',
					'./favicon.ico',
					'./html2canvas.min.js',
					'https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css',
					'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js',
					'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js',
					'https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js',
					'./reportLighthouse.js'
				])
			)
	)
});

self.addEventListener('fetch', event => {
	event.respondWith(
		caches.match(event.request, {cacheName, ignoreVary: false}).then(response => {
			return new Promise(resolve => {
				fetch(event.request).then(
					realResponse => resolve(realResponse), () => {
						if (response) {
							return resolve(response);
						}
					});
			});
		})
	)
});
