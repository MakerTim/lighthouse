<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>LightHouse Bug Collector</title>
	<meta name="description" content="Collection of all your bugs in one overview">
	<meta name="keywords" content="bug bugcollection bugreport bugs">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="revisit-after" content="7 days">
	<meta name="robots" content="all">
	<meta name="theme-color" content="#f44336">
	<link rel="apple-touch-icon" sizes="192x192" href="ios-icon.png">
	<link rel="icon" type="image/x-icon" href="./favicon.ico">
</head>

<script>
	const url = './';
</script>

<script>
	window.convertToSHA256 = function (data) {
		const rotateRight = function (n, x) {
			return ((x >>> n) | (x << (32 - n)));
		};
		const choice = function (x, y, z) {
			return ((x & y) ^ (~x & z));
		};

		function majority(x, y, z) {
			return ((x & y) ^ (x & z) ^ (y & z));
		}

		function sha256_Sigma0(x) {
			return (rotateRight(2, x) ^ rotateRight(13, x) ^ rotateRight(22, x));
		}

		function sha256_Sigma1(x) {
			return (rotateRight(6, x) ^ rotateRight(11, x) ^ rotateRight(25, x));
		}

		function sha256_sigma0(x) {
			return (rotateRight(7, x) ^ rotateRight(18, x) ^ (x >>> 3));
		}

		function sha256_sigma1(x) {
			return (rotateRight(17, x) ^ rotateRight(19, x) ^ (x >>> 10));
		}

		function sha256_expand(W, j) {
			return (W[j & 0x0f] += sha256_sigma1(W[(j + 14) & 0x0f]) + W[(j + 9) & 0x0f] +
				sha256_sigma0(W[(j + 1) & 0x0f]));
		}

		/* Hash constant words K: */
		const K256 = [0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5,
			0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
			0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
			0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
			0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
			0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
			0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
			0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
			0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
			0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
			0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3,
			0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
			0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5,
			0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
			0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
			0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2];

		/* global arrays */
		let ihash, count, buffer;
		const sha256_hex_digits = "0123456789abcdef";

		/* Add 32-bit integers with 16-bit operations (bug in some JS-interpreters:
		 overflow) */
		function safe_add(x, y) {
			const lsw = (x & 0xffff) + (y & 0xffff);
			const msw = (x >> 16) + (y >> 16) + (lsw >> 16);
			return (msw << 16) | (lsw & 0xffff);
		}

		/* Initialise the SHA256 computation */
		function sha256_init() {
			ihash = new Array(8);
			count = new Array(2);
			buffer = new Array(64);
			count[0] = count[1] = 0;
			ihash[0] = 0x6a09e667;
			ihash[1] = 0xbb67ae85;
			ihash[2] = 0x3c6ef372;
			ihash[3] = 0xa54ff53a;
			ihash[4] = 0x510e527f;
			ihash[5] = 0x9b05688c;
			ihash[6] = 0x1f83d9ab;
			ihash[7] = 0x5be0cd19;
		}

		/* Transform a 512-bit message block */
		function sha256_transform() {
			let a, b, c, d, e, f, g, h, T1, T2;
			const W = new Array(16);

			/* Initialize registers with the previous intermediate value */
			a = ihash[0];
			b = ihash[1];
			c = ihash[2];
			d = ihash[3];
			e = ihash[4];
			f = ihash[5];
			g = ihash[6];
			h = ihash[7];

			/* make 32-bit words */
			for (let i = 0; i < 16; i++)
				W[i] = ((buffer[(i << 2) + 3]) | (buffer[(i << 2) + 2] << 8) | (buffer[(i << 2) + 1]
					<< 16) | (buffer[i << 2] << 24));

			for (let j = 0; j < 64; j++) {
				T1 = h + sha256_Sigma1(e) + choice(e, f, g) + K256[j];
				if (j < 16) T1 += W[j];
				else T1 += sha256_expand(W, j);
				T2 = sha256_Sigma0(a) + majority(a, b, c);
				h = g;
				g = f;
				f = e;
				e = safe_add(d, T1);
				d = c;
				c = b;
				b = a;
				a = safe_add(T1, T2);
			}

			/* Compute the current intermediate hash value */
			ihash[0] += a;
			ihash[1] += b;
			ihash[2] += c;
			ihash[3] += d;
			ihash[4] += e;
			ihash[5] += f;
			ihash[6] += g;
			ihash[7] += h;
		}

		/* Read the next chunk of data and update the SHA256 computation */
		function sha256_update(data, inputLen) {
			let i, j, index, curpos = 0;
			/* Compute number of bytes mod 64 */
			index = ((count[0] >> 3) & 0x3f);
			const remainder = (inputLen & 0x3f);

			/* Update number of bits */
			if ((count[0] += (inputLen << 3)) < (inputLen << 3)) count[1]++;
			count[1] += (inputLen >> 29);

			/* Transform as many times as possible */
			for (i = 0; i + 63 < inputLen; i += 64) {
				for (j = index; j < 64; j++)
					buffer[j] = data.charCodeAt(curpos++);
				sha256_transform();
				index = 0;
			}

			/* Buffer remaining input */
			for (j = 0; j < remainder; j++)
				buffer[j] = data.charCodeAt(curpos++);
		}

		/* Finish the computation by operations such as padding */
		function sha256_final() {
			let i, index = ((count[0] >> 3) & 0x3f);
			buffer[index++] = 0x80;
			if (index <= 56) {
				for (i = index; i < 56; i++)
					buffer[i] = 0;
			} else {
				for (i = index; i < 64; i++)
					buffer[i] = 0;
				sha256_transform();
				for (i = 0; i < 56; i++)
					buffer[i] = 0;
			}
			buffer[56] = (count[1] >>> 24) & 0xff;
			buffer[57] = (count[1] >>> 16) & 0xff;
			buffer[58] = (count[1] >>> 8) & 0xff;
			buffer[59] = count[1] & 0xff;
			buffer[60] = (count[0] >>> 24) & 0xff;
			buffer[61] = (count[0] >>> 16) & 0xff;
			buffer[62] = (count[0] >>> 8) & 0xff;
			buffer[63] = count[0] & 0xff;
			sha256_transform();
		}

		/* Get the internal hash as a hex string */
		function sha256_encode_hex() {
			let output = "";
			for (var i = 0; i < 8; i++) {
				for (var j = 28; j >= 0; j -= 4)
					output += sha256_hex_digits.charAt((ihash[i] >>> j) & 0x0f);
			}
			return output;
		}

		sha256_init();
		sha256_update(data, data.length);
		sha256_final();
		return sha256_encode_hex();

	};
</script>

<style>
	.background-square {
		display: block;
		width: 100%;
		height: 100%;
		min-height: 40px;
		position: absolute;
		-webkit-box-shadow: 2px 6px 31px 6px rgba(0, 0, 0, 0.1);
		-moz-box-shadow: 2px 6px 31px 6px rgba(0, 0, 0, 0.1);
		box-shadow: 2px 6px 31px 6px rgba(0, 0, 0, 0.1);
	}

	h1 {
		cursor: default;
	}

	body > .row {
		margin-left: 0;
		margin-right: 0;
		height: 100vh;
		overflow: auto;
	}

	.headerRow {
		background-color: #f44336;
		color: white;
		text-shadow: 1px 1px 1px black;
		text-align: center;
		margin-bottom: 0;
		padding-bottom: 5px;
		border-top-left-radius: 8px;
		border-top-right-radius: 8px;
	}

	.block {
		border-top-left-radius: 10px;
		border-top-right-radius: 10px;
		border: 2px solid rgba(0, 0, 0, 0.2);
		border-top-color: rgba(0, 0, 0, 0.1);
		border-left-color: rgba(0, 0, 0, 0.1);
		margin-top: 10px;
		background-color: whitesmoke;
	}

	.btn {
		text-shadow: 1px 1px 1px black;
	}

	button.btn-primary {
		background-color: #0072ec;
		border-color: #0072ec;
	}

	.request {
		font-size: 0.6em;
		float: right;
		display: block
		padding-top: 4px;
		color: black;
		text-decoration: black;
	}

	.hidden {
		display: none !important;
	}

	.block > div {
		margin: 10px;
	}

	.incidents > div:not(.hoverScroll) {
		min-width: 100%;
		display: inline-block;
		word-wrap: break-word;
		overflow: hidden;
	}

	#Bug-Blocks > div {
		overflow: hidden;
	}

	#Bug-Blocks > div > div > span, .incidents > div > span {
		white-space: nowrap;
	}

	.incidents {
		width: 100%;
		display: flex;
		margin-bottom: -15px;
		height: calc(15em + 5px);
	}

	.screenshot {
		width: auto;
		height: auto;
		max-width: 100%;
		max-height: 1.5em;
	}

	.hoverScroll {
		height: calc(15rem - 12px);
		position: absolute;
		width: 20%;
		font-size: 5em;
		text-align: center;
		padding-top: 0.3em;
		user-select: none;
		color: transparent;
		text-shadow: none;
	}

	.hoverScroll:hover {
		background-color: rgba(255, 255, 255, 0.5);
		border-top: 5px solid white;
		color: darkorange;
		box-sizing: border-box;
		text-shadow: rgba(0, 0, 0, 0.1) 1px 1px 0;
	}

	.hover-left {
		border-top-left-radius: 10px;
	}

	.hover-left:hover {
		border-left: 1px solid white;
	}

	.hover-right {
		right: 15px;
		border-top-right-radius: 10px;
	}

	.hover-right:hover {
		border-right: 1px solid white;
	}

	hr {
		margin: 4px;
	}
</style>
<body>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="./html2canvas.min.js"></script>
<script src="./reportLighthouse.js"></script>

<span class="background-wrapper"
      style="display: block;width: 100%;height: 100%;overflow: hidden;position: absolute;top: 0;left: 0;">
    <span class="background-square"
          style="background-color: #eff; transform: rotate(0deg); right: 0; box-shadow: rgba(0, 0, 0, 0.1) 19.1px -0.03px 31px 6px;"></span>
    <span class="background-square"
          style="background-color: #dff; transform: rotate(140deg); right: 192px; box-shadow: rgba(0, 0, 0, 0.1) 19.1px -0.03px 31px 6px;"></span>
    <span class="background-square"
          style="background-color: #cff; transform: rotate(280deg); right: 384px; box-shadow: rgba(0, 0, 0, 0.1) 19.1px -0.03px 31px 6px;"></span>
    <span class="background-square"
          style="background-color: #bff; transform: rotate(60deg); right: 576px; box-shadow: rgba(0, 0, 0, 0.1) 19.1px -0.03px 31px 6px;"></span>
    <span class="background-square"
          style="background-color: #aff; transform: rotate(200deg); right: 768px; box-shadow: rgba(0, 0, 0, 0.1) 19.1px -0.03px 31px 6px;"></span>
</span>

<div class="row">
	<div class="col-md-2"></div>
	<div class="col-md-8">
		<div class="block">
			<h1 class="headerRow">LightHouse Bug Collector</h1>
			<div>
				<div class="form-group">
					<label for="Application-Select">Application:</label>
					<a href="mailto:MakerTim@outlook.com?subject=LightHouse Application Request&body=Request for registration application: ___"
					   class="request">
						Request Application
					</a>
					<select id="Application-Select" class="form-control">
						<option selected disabled>Select Application</option>
					</select>
				</div>
				<div class="form-group">
					<label for="Secret-Key">Password:</label>
					<input id="Secret-Key" type="password" class="form-control">
				</div>
				<button type="submit" class="btn btn-primary" id="Collect-Button">Collect</button>
			</div>
		</div>

		<div class="block hidden">
			<h1 class="headerRow">Found bugs</h1>
			<div id="Bug-Blocks" class="row">

			</div>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>

<script>
	$(document).ready(() => {
		const select = $('#Application-Select');

		// Get all Applications
		$.ajax({
			dataType: "json",
			url,
			headers: {
				Application: ''
			},
			data: {},
			success(response) {
				const order = response['_Order'];
				order.forEach((key) => {
					select.append($('<option></option>')
						.attr('value', response[key].name)
						.text(response[key].name));
				});
			}
		});

		// On "Login"
		$('#Collect-Button').click(() => {
			$.ajax({
				dataType: "json",
				url,
				headers: {
					Application: select.val(),
					Secret: window.convertToSHA256($('#Secret-Key').val())
				},
				data: {},
				success(response) {
					$('#Collect-Button').text('Refresh');
					$('.Count-Block').remove();
					const blocks = $('#Bug-Blocks');
					blocks.children().remove();


					blocks.parent().removeClass('hidden');
					const order = response['_Order'];
					delete response['_Order'];
					delete response['_Count'];
					let count = 0;
					order.forEach((key) => {
						if (!response[key].Solved_Since) {
							count++;
						}
					});
					blocks.parent().children().first().after($('<div></div>')
						.addClass('Count-Block')
						.text('Open bugs: ' + count));

					order.forEach((key) => {
						const report = response[key];
						const divBlock = $('<div></div>')
							.append(
								$('<button></button>')
									.text('✘')
									.attr('title', 'Delete')
									.css('float', 'right')
									.addClass('btn btn-outline-danger btn-sm')
									.click(() => {
										$.ajax({
											dataType: "json",
											url,
											method: 'POST',
											headers: {
												Application: select.val(),
												Secret: window.convertToSHA256($('#Secret-Key').val()),
												Handle: 'DELETE',
												Rid: key,
											},
											data: {},
											success() {
												$('#Collect-Button').click();
											}
										});
									})
							);
						if (report.Solved_Since) {
							divBlock.append(
								$('<button></button>')
									.text('✪')
									.attr('title', 'Mark unsolved')
									.css('float', 'right')
									.css('color', 'darkorange')
									.css('border-color', 'darkorange')
									.addClass('btn btn-outline-warning btn-sm')
									.click(() => {
										$.ajax({
											dataType: "json",
											url,
											method: 'POST',
											headers: {
												Application: select.val(),
												Secret: window.convertToSHA256($('#Secret-Key').val()),
												Handle: 'UNSOLVE',
												Rid: key,
											},
											data: {},
											success() {
												$('#Collect-Button').click();
											}
										});
									})
							);
						} else {
							divBlock.append(
								$('<button></button>')
									.text('✓')
									.attr('title', 'Mark Solved')
									.css('float', 'right')
									.addClass('btn btn-outline-success btn-sm')
									.click(() => {
										$.ajax({
											dataType: "json",
											url,
											method: 'POST',
											headers: {
												Application: select.val(),
												Secret: window.convertToSHA256($('#Secret-Key').val()),
												Handle: 'SOLVE',
												Rid: key,
											},
											data: {},
											success() {
												$('#Collect-Button').click();
											}
										});
									})
							)
						}
						divBlock
							.append(
								$('<h2></h2>')
									.text('R' + key + ' - ' + report.Type)
									.addClass('title')
							)
							.append($('<hr>'))
							.addClass('col-lg-4')
							.css('padding', '15px');
						if (report.Solved_Since) {
							divBlock
								.css('color', 'green')
								.css('background-color', 'rgba(0, 0, 0, 0.01)');
						} else {
							divBlock
								.css('color', 'darkorange')
								.css('text-shadow', '1px 1px 0px rgba(0, 0, 0, 0.1)')
								.css('background-color', 'rgba(0, 0, 0, 0.1)');
						}

						const properties = ['Message', 'File', 'Line', 'Stacktrace', 'Solved_Since'];
						for (const property of properties) {
							divBlock.append(
								$('<div></div>')
									.append(
										$('<span></span>')
											.append(
												$('<b></b>')
													.text(property + ': ')
													.css('user-select', 'none')
											)
											.append(report[property])
											.attr('title', report[property])
									)
							);
						}
						const incidentsBlock = $('<div></div>')
							.append($('<div></div>').addClass('hoverScroll hover-right').text('>'))
							.append($('<div></div>').addClass('hoverScroll hover-left').text('<'));
						divBlock.append(
							$('<div></div>')
								.append(
									$('<span></span>')
										.append(
											$('<b></b>')
												.text('Incidents: ')
												.css('user-select', 'none')
										)
										.append(report.Incidents_Order.length)
										.attr('title', report.Incidents_Order.length)
								)
								.append(
									$('<button></button>')
										.text('▼')
										.css('margin-left', '5px')
										.css('border', '0')
										.css('padding', '0 5px')
										.addClass('btn btn-outline-secondary btn-sm')
										.click((event) => {
											const btnTxt = $(event.target).text();
											if (btnTxt === '▼') {
												$(event.target).text('▲');
												incidentsBlock
													.removeClass('hidden')
											} else {
												$(event.target).text('▼');
												incidentsBlock
													.addClass('hidden')
											}
										})
								)
						);
						for (const key of report.Incidents_Order) {
							const incident = report.Incidents[key];
							const incidentHTML = $('<div></div>')
								.append('<hr>');
							const properties = ['When', 'Environment', 'Agent', 'Origin', 'Meta', 'Version', 'From'];
							for (const property of properties) {
								incidentHTML
									.append(
										$('<span></span>')
											.append(
												$('<b></b>')
													.text(property + ': ')
													.css('user-select', 'none')
											)
											.append(incident[property])
											.attr('title', incident[property])
									)
									.append('<br>')
							}
							if (incident.Screenshot) {
								incidentHTML
									.append(
										$('<span></span>')
											.css('text-align', 'center')
											.css('display', 'block')
											.append(
												$('<b></b>')
													.text('Screenshot: ')
													.css('user-select', 'none')
											)
											.append(
												$('<img>')
													.addClass('screenshot')
													.attr('src', incident.Screenshot)
													.attr('title', 'Screenshot')
													.attr('alt', 'Screenshot')
													.css('cursor', 'pointer')
													.click(() => {
														window.open().document.body.innerHTML =
															'<img src="' + incident.Screenshot + '" alt="Missing screenshot"' +
															'style="width: auto;height: auto;max-width: calc(100% + 16px);max-height: 100%; margin: 0 -8px;">'
													})
											)
									)
									.append('<br>')
							}

							incidentsBlock.append(incidentHTML)
						}
						divBlock.append(
							incidentsBlock
								.attr('lastScroll', '0')
								.on('scroll', (event) => {
									if (event.target.scrollLeft > $(event.target).attr('lastScroll')) {
										event.target.scrollLeft = parseInt($(event.target).attr('lastScroll')) + event.target.offsetWidth;
									} else if (event.target.scrollLeft < $(event.target).attr('lastScroll')) {
										event.target.scrollLeft = parseInt($(event.target).attr('lastScroll')) - event.target.offsetWidth;
									}
									$(event.target).attr('lastScroll', event.target.scrollLeft.toString());
								})
								.click((event) => {
									if (event.target.classList.contains('hover-left')) {
										incidentsBlock[0].scrollLeft -= incidentsBlock.width();
									} else if (event.target.classList.contains('hover-right')) {
										incidentsBlock[0].scrollLeft += incidentsBlock.width();
									}
								})
								.addClass('hidden incidents')
								.css('overflow', 'auto')
						);
						blocks.append(divBlock);
					});
				}
			});
		});
	});
</script>

<script>
	$(document).ready(() => {
		if (!'serviceWorker' in Navigator) {
			console.warn('No Offline Support');
			return;
		}
		navigator.serviceWorker.register('sw.js')
			.then(() => {
			}, err => {
				console.error(err)
			})
	});
</script>

</body>
</html>
