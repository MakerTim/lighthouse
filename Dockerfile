FROM docker.io/richarvey/nginx-php-fpm:latest

# Copy all local files into the image.
WORKDIR /var/www/html

ADD conf_nginx-site.conf /etc/nginx/sites-available/default.conf
COPY . .
