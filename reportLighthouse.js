{
	const Application = 'Lighthouse';
	const Key = '0b8e9e995d8d77f1e4770f0f79665aee6f3f70247b3735422daba73df4c3096f';
	const Version = '1.0';
	const Meta = 'Admin.php';
	const Environment = '';

	let mouseX = 0;
	let mouseY = 0;

	window.onmousemove = evt => {
		mouseX = evt.clientX;
		mouseY = evt.clientY;
	};

	window.onerror = (errorEvent, File, lineno, colno, Stacktrace) => {
		const width = 5;
		const mouseOffsetX = mouseX + Math.ceil(width / 2);
		const mouseOffsetY = mouseY + Math.ceil(width / 2);
		let promise;
		if (typeof html2canvas === 'function') {
			promise = html2canvas(document.body, {logging: false});
		} else {
			promise = new Promise(resolve => resolve(null));
		}
		promise.then(function (canvas) {
			let Screenshot = null;
			try { // Add cursor to error
				const context = canvas.getContext("2d");
				const shadow = 2;
				const radius = 15;
				['black', 'white'].forEach((color, index) => {
					context.beginPath();
					context.strokeStyle = color;
					context.lineWidth = width - (index * shadow);
					context.arc(mouseOffsetX, mouseOffsetY, radius, 0, Math.PI / 2);
					context.moveTo(mouseOffsetX - (shadow - index), mouseOffsetY);
					context.lineTo(mouseOffsetX + radius + (shadow - index), mouseOffsetY);
					context.moveTo(mouseOffsetX, mouseOffsetY - (shadow - index));
					context.lineTo(mouseOffsetX, mouseOffsetY + radius + (shadow - index));
					context.stroke();
				});
				Screenshot = canvas.toDataURL();
			} catch (e) {
				console.log('Error with screenshot', e);
			}

			const Type = errorEvent.split(':')[0];
			const Line = lineno + '.' + colno;
			let Message = '';
			if (Stacktrace) {
				Stacktrace = Stacktrace.stack.split('\n');
				Message = Stacktrace.shift().split(':')[1];
				Stacktrace = Stacktrace.join('\n');
			}

			$.ajax({
				method: 'POST',
				url: './',
				crossDomain: true,
				dataType: 'json',
				headers: {
					Application,
					Key,
					Incident: 1,
					'Content-Type': 'application/json' // optional
				},
				data: JSON.stringify({
					Type,
					Message,
					File,
					Line,
					Stacktrace,
					Screenshot,

					Environment,
					Meta,
					Version
				}),
				success(response) {
					try {
						const id = response.ID;
						const idParsed = atob(id);
						console.warn('An error has occurred!\n   Reference id = ' + id + ' (' + idParsed + ')');
					} catch (e) {
						console.warn('An error has occurred!\n' + response);
					}
				}
			});
		});
		return true;
	}
}
