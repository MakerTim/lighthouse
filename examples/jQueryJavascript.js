console.warn('This is an example file, please dont use this in production!');

{
	const Application = 'Test';
	const Key = 'df348abec3fe9d1117f4514a3281fb836cc0589b394f5d408adbbc162dbe9abc';
	const Version = '1.0';
	const Meta = 'jQuery';
	const Environment = 'TEST';

	window.onerror = (errorEvent, File, lineno, colno, Stacktrace) => {
		const Type = errorEvent.split(':')[0];
		const Line = lineno + '.' + colno;
		const Message = '';
		if (Stacktrace) {
			Stacktrace = Stacktrace.stack.split('\n');
			const Message = Stacktrace.shift().split(':')[1];
			Stacktrace = Stacktrace.join('\n');
		}

		$.ajax({
			method: 'POST',
			url: 'https://lighthouse.makertim.nl',
			crossDomain: true,
			dataType: 'json',
			headers: {
				Application,
				Key,
				Incident: 1,
				'Content-Type': 'applicatoin/json' // optional
			},
			data: JSON.stringify({
				Type,
				Message,
				File,
				Line,
				Stacktrace,

				Environment,
				Meta,
				Version
			}),
			success(response) {
				try {
					const id = response.ID;
					const idParsed = atob(id);
					console.warn('An error has occurred!\n   Reference id = ' + id + ' (' + idParsed + ')');
				} catch (e) {
					console.warn('An error has occurred!\n' + response);
				}
			}
		});
		return true;
	}
}
console.warn(MakerTimVariableTest);
