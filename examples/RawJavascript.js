console.warn('This is an example file, please dont use this in production!');

{
	const Application = 'Test';
	const Key = 'df348abec3fe9d1117f4514a3281fb836cc0589b394f5d408adbbc162dbe9abc';
	const Version = '1.0';
	const Meta = 'RawJavascript';
	const Environment = 'TEST';

	window.onerror = (errorEvent, File, lineno, colno, Stacktrace) => {
		const Type = errorEvent.split(':')[0];
		const Line = lineno + '.' + colno;
		const Message = '';
		if (Stacktrace) {
			Stacktrace = Stacktrace.stack.split('\n');
			const Message = Stacktrace.shift().split(':')[1];
			Stacktrace = Stacktrace.join('\n');
		}

		const xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = (event) => {
			if (event.currentTarget.readyState === 4) {
				try {
					const id = JSON.parse(event.currentTarget.response).ID;
					const idParsed = atob(id);
					console.warn('An error has occurred!\n   Reference id = ' + id + ' (' + idParsed + ')');
				} catch (e) {
					console.warn('An error has occurred!\n' + event.currentTarget.response);
				}
			}
		};
		xhttp.open('POST', 'https://lighthouse.makertim.nl', true);
		xhttp.setRequestHeader('Application', Application);
		xhttp.setRequestHeader('Key', Key);
		xhttp.setRequestHeader('Incident', '1');
		xhttp.setRequestHeader('Content-Type', 'application/json'); // optional
		xhttp.send(JSON.stringify({
			Type,
			Message,
			File,
			Line,
			Stacktrace,

			Environment,
			Meta,
			Version
		}));
		return true;
	}
}
console.warn(MakerTimVariableTest);
