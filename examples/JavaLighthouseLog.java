import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class LighthouseLog {

    public static ExecutorService executor = Executors.newFixedThreadPool(1);
    public static String version = "1.0";
    public static String environment = System.getProperty("java.version");
    public static String meta = ManagementFactory.getRuntimeMXBean().getVmVersion();

    public static void main(String[] args) {
        log(new Exception("Java Error Message"));
    }

    private static String save(String in) {
        if (in == null) {
            return null;
        }
        return '"' + in
                .replaceAll("\"", "\\\"")
                .replaceAll("\n", "\\\\n") + '"';
    }

    private static String save(StackTraceElement[] in) {
        if (in == null) {
            return null;
        }
        StringBuilder buildString = new StringBuilder();
        for (StackTraceElement stackTraceElement : in) {
            buildString.append("    at ");
            buildString.append(stackTraceElement.getFileName());
            buildString.append(" (");
            buildString.append(stackTraceElement.getClassName());
            buildString.append(".");
            buildString.append(stackTraceElement.getMethodName());
            buildString.append("():");
            buildString.append(stackTraceElement.getLineNumber());
            buildString.append(")\n");
        }
        return save(buildString.toString());
    }

    public static void log(Throwable throwable) {
        StackTraceElement[] traceElements = throwable.getStackTrace();

        executor.submit(() -> {
            CloseableHttpClient client = HttpClientBuilder.create().build();
            try {
                HttpPost post = new HttpPost("https://lighthouse.makertim.nl");
                post.addHeader("Application", "Test");
                post.addHeader("Key", "df348abec3fe9d1117f4514a3281fb836cc0589b394f5d408adbbc162dbe9abc");
                post.addHeader("Incident", "1");
                post.addHeader("Content-Type", "application/json"); // optional
                String body = "{" +
                        "\"Type\":" + save(throwable.getClass().getCanonicalName()) + "," +
                        "\"Message\":" + save(throwable.getMessage()) + "," +
                        "\"File\":" + save(traceElements[0].getFileName() + " (" + traceElements[0].getMethodName() + "())") + "," +
                        "\"Line\":" + save("" + traceElements[0].getLineNumber()) + "," +
                        "\"Stacktrace\":" + save(traceElements) + "," +
                        "\"Environment\":" + save(environment) + "," +
                        "\"Meta\":" + save(meta) + "," +
                        "\"Version\":" + save(version) + "" +
                        "}";
                post.setEntity(new StringEntity(body));

                client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
